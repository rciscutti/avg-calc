# README #

avg-calc is a desktop application written in Java that manages an electronic register (record book) for university students. A student can add a new exam to the register, remove an existing exam or all exams. avg-calc also computes some statistics: average of the exams passed, total number of CFUs, number of exams passed, (hypothetical) degree vote, and whether the student has passed a number of exams sufficient to defend his/her thesis.

## Fixing issues ##

Create a commit (or more commits, if needed) to fix each issue. Remember to mention, in the commit message, the issue you are working on -- each issue has an ID. 
